<?php
/*
Plugin Name: Primary Category
Description: Allows publishers to designate a primary category for posts. It adds a Make Primary link to categories on edit post meta box. And saves it on wp_postmeta table on key 'tenup_primary_category'
Author: Juan Carlos Contreras Ontiveros
Version: 1.0
*/

add_action( 'wp_enqueue_scripts', 'tenup_scripts' );
function tenup_scripts()
{
    wp_register_script( 'tenup-script', plugins_url( '/js/tenup-script.js', __FILE__ ), array('jquery') );
    wp_enqueue_script( 'tenup-script' );
}

add_action( 'wp_enqueue_style', 'tenup_styles' );
function tenup_styles()
{
    wp_register_style( 'tenup-style', plugins_url( '/css/tenup-style.css', __FILE__ ) );
    wp_enqueue_style( 'tenup-style' );
}

// This is required to be sure Walker_Category_Checklist class is available
require_once ABSPATH . 'wp-admin/includes/template.php';

/**
 * Custom walker to print category checkboxes with Make main link, Copy from original walker
 */
class tenup_Walker_Category_Checklist extends Walker_Category_Checklist {
	public $tree_type = 'category';
	public $db_fields = array ('parent' => 'parent', 'id' => 'term_id'); //TODO: decouple this

	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent<ul class='children'>\n";
	}

	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}

	public function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
		if ( empty( $args['taxonomy'] ) ) {
			$taxonomy = 'category';
		} else {
			$taxonomy = $args['taxonomy'];
		}

		if ( $taxonomy == 'category' ) {
			$name = 'post_category';
		} else {
			$name = 'tax_input[' . $taxonomy . ']';
		}

		$args['popular_cats'] = empty( $args['popular_cats'] ) ? array() : $args['popular_cats'];
		$class = in_array( $category->term_id, $args['popular_cats'] ) ? ' class="popular-category"' : '';

		$args['selected_cats'] = empty( $args['selected_cats'] ) ? array() : $args['selected_cats'];

		if ( ! empty( $args['list_only'] ) ) {
			$aria_cheched = 'false';
			$inner_class = 'category';

			if ( in_array( $category->term_id, $args['selected_cats'] ) ) {
				$inner_class .= ' selected';
				$aria_cheched = 'true';
			}

			/** This filter is documented in wp-includes/category-template.php */
			$output .= "\n" . '<li' . $class . '>' .
				'<div class="' . $inner_class . '" data-term-id=' . $category->term_id .
				' tabindex="0" role="checkbox" aria-checked="' . $aria_cheched . '">' .
				esc_html( apply_filters( 'the_category', $category->name ) ) . '</div>';
		} else {
			/** This filter is documented in wp-includes/category-template.php */
                        $linkBefore = __( 'Make primary' );
                        $linkAfter = __( 'Is Primary' );
			$output .= "\n<li id='{$taxonomy}-{$category->term_id}'$class>" .
				'<label class="selectit"><input value="' . $category->term_id . '" type="checkbox" name="'.$name.'[]" id="in-'.$taxonomy.'-' . $category->term_id . '"' .
				checked( in_array( $category->term_id, $args['selected_cats'] ), true, false ) .
				disabled( empty( $args['disabled'] ), false, false ) . ' /> ' .
				esc_html( apply_filters( 'the_category', $category->name ) ) . '</label>';
                        
                        //Get post meta key from post and compare to the current category id
                        if( $category->term_id == get_post_meta(get_the_ID(), 'tenup_primary_category', true) ) {
                            $output .= '<span class="primary-category-selected" name="label-primary-category" onClick="tenup_make_primary(this, ' . $category->term_id . ', \'' . $linkBefore . '\', \'' . $linkAfter . '\');">' . $linkAfter . '</span>';
                        } else {
                            $output .= '<span class="primary-category-unselected" name="label-primary-category" onClick="tenup_make_primary(this, ' . $category->term_id . ', \'' . $linkBefore . '\', \'' . $linkAfter . '\');">' . $linkBefore . '</span>';
                        }
		}
	}

	public function end_el( &$output, $category, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}
}

add_action('add_meta_boxes', 'tenup_modify_meta_box');
function tenup_modify_meta_box( $post_type ) {
  remove_meta_box( 'categorydiv', $post_type, 'side' );
  tenup_scripts();
  tenup_styles();
  add_meta_box( 'categorydiv', __( 'Categories' ), 'tenup_primary_category_meta_box', $post_type, 'side' );
}

/**
 * Custom category meta box copied from original wp_admin/includes/meta-boxes.php
 * Method post_tags_meta_box()
 */
function tenup_primary_category_meta_box($post, $meta_box){
    $walker = new tenup_Walker_Category_Checklist();

    $defaults = array( 'taxonomy' => 'category' );
    if ( ! isset( $box['args'] ) || ! is_array( $box['args'] ) ) {
        $args = array();
    } else {
        $args = $box['args'];
    }
    $r = wp_parse_args( $args, $defaults );
    $tax_name = esc_attr( $r['taxonomy'] );
    $taxonomy = get_taxonomy( $r['taxonomy'] );
    ?>

    <div id="taxonomy-<?php echo $tax_name; ?>" class="categorydiv">
        <ul id="<?php echo $tax_name; ?>-tabs" class="category-tabs">
            <li class="tabs"><a href="#<?php echo $tax_name; ?>-all"><?php echo $taxonomy->labels->all_items; ?></a></li>
            <li class="hide-if-no-js"><a href="#<?php echo $tax_name; ?>-pop"><?php _e( 'Most Used' ); ?></a></li>
        </ul>

        <div id="<?php echo $tax_name; ?>-pop" class="tabs-panel" style="display: none;">
            <ul id="<?php echo $tax_name; ?>checklist-pop" class="categorychecklist form-no-clear" >
                <?php $popular_ids = wp_popular_terms_checklist( $tax_name ); ?>
            </ul>
        </div>

        <div id="<?php echo $tax_name; ?>-all" class="tabs-panel">
            <?php
            $name = ( $tax_name == 'category' ) ? 'post_category' : 'tax_input[' . $tax_name . ']';
            echo "<input type='hidden' name='{$name}[]' value='0' />"; // Allows for an empty term set to be sent. 0 is an invalid Term ID and will be ignored by empty() checks.
            ?>
            <ul id="<?php echo $tax_name; ?>checklist" data-wp-lists="list:<?php echo $tax_name; ?>" class="categorychecklist form-no-clear">
                <!-- Input for sending the primary category -->
                <input type='hidden' id='primary-category-id' name='primary-category-id' value=''>
                <?php wp_terms_checklist( $post->ID, array( 'taxonomy' => $tax_name, 'popular_cats' => $popular_ids, 'walker' => $walker ) ); ?>
            </ul>
        </div>
        <?php if ( current_user_can( $taxonomy->cap->edit_terms ) ) : ?>
            <div id="<?php echo $tax_name; ?>-adder" class="wp-hidden-children">
                <a id="<?php echo $tax_name; ?>-add-toggle" href="#<?php echo $tax_name; ?>-add" class="hide-if-no-js taxonomy-add-new">
                        <?php
                            /* translators: %s: add new taxonomy label */
                            printf( __( '+ %s' ), $taxonomy->labels->add_new_item );
                        ?>
                </a>
                <p id="<?php echo $tax_name; ?>-add" class="category-add wp-hidden-child">
                    <label class="screen-reader-text" for="new<?php echo $tax_name; ?>"><?php echo $taxonomy->labels->add_new_item; ?></label>
                    <input type="text" name="new<?php echo $tax_name; ?>" id="new<?php echo $tax_name; ?>" class="form-required form-input-tip" value="<?php echo esc_attr( $taxonomy->labels->new_item_name ); ?>" aria-required="true"/>
                    <label class="screen-reader-text" for="new<?php echo $tax_name; ?>_parent">
                        <?php echo $taxonomy->labels->parent_item_colon; ?>
                    </label>
                    <?php
                    $parent_dropdown_args = array(
                        'taxonomy'         => $tax_name,
                        'hide_empty'       => 0,
                        'name'             => 'new' . $tax_name . '_parent',
                        'orderby'          => 'name',
                        'hierarchical'     => 1,
                        'show_option_none' => '&mdash; ' . $taxonomy->labels->parent_item . ' &mdash;',
                    );
                    $parent_dropdown_args = apply_filters( 'post_edit_category_parent_dropdown_args', $parent_dropdown_args );
                    wp_dropdown_categories( $parent_dropdown_args );
                    ?>
                    <input type="button" id="<?php echo $tax_name; ?>-add-submit" data-wp-lists="add:<?php echo $tax_name; ?>checklist:<?php echo $tax_name; ?>-add" class="button category-add-submit" value="<?php echo esc_attr( $taxonomy->labels->add_new_item ); ?>" />
                    <?php wp_nonce_field( 'add-' . $tax_name, '_ajax_nonce-add-' . $tax_name, false ); ?>
                    <span id="<?php echo $tax_name; ?>-ajax-response"></span>
                </p>
            </div>
        <?php endif; ?>
    </div>
    <?php
}

//Redordering Meta Boxes
add_filter( 'get_user_option_meta-box-order_post', 'metabox_order' );
function metabox_order( $order ) {
    return array(
        'side' => join( 
            ",", 
            array(
                'submitdiv',
                'formatdiv',
                'categorydiv',
            )
        ),
    );
}

add_action( 'save_post', 'tenup_save_post_meta', 10, 2 );
function tenup_save_post_meta( $post_id, $post ) {
	/* 
	 * Check current user permissions
	 */
	$post_type = get_post_type_object( $post->post_type );
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;
 
        update_post_meta($post_id, 'tenup_primary_category', trim( $_POST['primary-category-id'] ) );
        
	return $post_id;
}

?>
