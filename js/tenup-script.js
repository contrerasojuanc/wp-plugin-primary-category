function tenup_make_primary(elem, value, linkBefore, linkAfter) {
	jQuery('#primary-category-id').val(value);
	jQuery('[name="label-primary-category"]').each(function (index, value){
	  jQuery(this).html(linkBefore);
	  jQuery(this).removeClass('primary-category-selected');
	  jQuery(this).addClass('primary-category-unselected');
	});
	jQuery(elem).html(linkAfter);
	jQuery(elem).removeClass('primary-category-unselected');
	jQuery(elem).addClass('primary-category-selected');
};